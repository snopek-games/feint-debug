extends Sprite

onready var sv = get_parent()
onready var animsprite = $"../svanimsprite"
onready var victimsprite = $"../svvictimsprite"
func _process(delta):
	if animsprite.is_playing() == false && victimsprite.is_playing() == false && sv.movingState == true && sv.crouchState == false:
		self.visible = true
	else:
		self.visible = false
		
