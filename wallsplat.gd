extends Area2D
signal attackconnected(attacktype)
onready var svvictimsprite = get_node("/root/Main/ServerPlayer/svvictimsprite")
onready var clvictimsprite = get_node("/root/Main/ClientPlayer/clvictimsprite")
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if svvictimsprite.is_playing() || clvictimsprite.is_playing():
		self.monitoring = true
		if self.overlaps_area($"/root/Main/ServerPlayer/svhurtbox"):
			emit_signal("attackconnected", "splat")
	else:
		self.monitoring = false
