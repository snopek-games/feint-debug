extends AnimatedSprite
onready var sv = get_parent()
onready var animsprite = $"../svanimsprite"
onready var victimsprite = $"../svvictimsprite"
func _process(delta):
	if sv.deadState == true:
		self.visible = true
		self.animation = "death"
		animsprite.stop()
		animsprite.visible = false
	if self.is_playing():
		animsprite.stop()
		self.visible = true
		animsprite.visible = false
	else:
		self.visible = false
		
