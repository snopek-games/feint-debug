extends SGKinematicBody2D
# local variables
var blockingState = true
var movingState = false
var posSwitch = false
var deadState = false
var counterState = false
var tooclose = false
var input_prefix := "player1_"
var behindYou = false
var distancelimit = SGFixed.ONE
# fixed point variables
var velocity = SGFixedVector2.new().copy()
#var axisVector = SGFixedVector2.new().copy()
var direction = SGFixed.NEG_ONE
var speed = 444444
var svrot = SGFixed.ONE
var clrot = SGFixed.ONE
#var xdiff = int(1)
#var ydiff = int(1)
var angle = int(1)
#var aVx = int(1)
#var aVy = int(1)
#var xdir = int(1)
#var ydir = int(1)
# node paths
onready var serverplayer = $"../ServerPlayer"
onready var clientplayer = $"../ClientPlayer"
onready var animsprite = $svanimsprite
onready var victimsprite = $svvictimsprite
onready var animtimer = $AttackTimer
onready var timer = $"/root/Main/RoundTimer"
onready var roundender = $"/root/Main/EndSplash/REControl/RoundEnder"
# signals
# bot variables
var neverblock = false
var neverspin = false
func _ready():
	self.fixed_position = SGFixed.vector2(16777216, 33554432).copy()
	set_global_fixed_rotation(angle)
func _save_state() -> Dictionary:
	return {
		svpos = self.get_global_fixed_position().copy(),
		velocity = velocity.copy(),
		direction = direction,
		isblocking = blockingState,
		ismoving = movingState,
		speed = speed,
		switched = posSwitch,
	}

func _load_state(state: Dictionary) -> void:
	self.set_global_fixed_position(state['svpos'].copy())
	velocity = state['velocity'].copy()
	direction = state['direction']
	blockingState = state['isblocking']
	movingState = state['ismoving']
	speed = state['speed']
	posSwitch = state['switched']
	sync_to_physics_engine()


func _get_local_input() -> Dictionary:
	var input_vector = Input.get_vector(input_prefix + "back", input_prefix + "forward", input_prefix +  "down", input_prefix +  "up")
	var input := {}
	if input_vector != Vector2.ZERO:
		input["input_vector"] = input_vector
	if Input.is_action_pressed(input_prefix +"crouch"):
		input["crouch"] = true
	if Input.is_action_just_pressed(input_prefix + "up") || Input.is_action_just_pressed(input_prefix + "back") || Input.is_action_just_released(input_prefix + "down") || Input.is_action_just_released(input_prefix + "forward"):
		input["ub"] = true
	if Input.is_action_just_pressed(input_prefix + "down") || Input.is_action_just_pressed(input_prefix + "forward") || Input.is_action_just_released(input_prefix + "up") || Input.is_action_just_released(input_prefix + "back"):
		input["df"] = true
	if Input.is_action_just_pressed(input_prefix + "light_attack"):
		input["la"] = true
	if Input.is_action_just_pressed(input_prefix + "heavy_attack"):
		input["ha"] = true
	return input

func _network_process(input: Dictionary) -> void:
	var input_vector = input.get("input_vector", Vector2.ZERO)
#	print(altvel.x)
#	print(float(angle)/65536*180/PI)
#	print(float(self.get_global_fixed_rotation())/65536*180/PI)
# use that to calculate the angle and distance between players
	angle = SGFixed.atan2(
		clientplayer.get_global_fixed_position().y - self.get_global_fixed_position().y,
		clientplayer.get_global_fixed_position().x - self.get_global_fixed_position().x
	)
	var distance = self.get_global_fixed_position().distance_to(clientplayer.get_global_fixed_position())
# use that to calculate the normalized vector between players
#	aVx = SGFixed.div(SGFixed.cos(angle), distance)
#	aVy = SGFixed.div(SGFixed.sin(angle), distance)
# alt physics
#	xdir = SGFixed.div(xdiff, distance)
#	ydir = SGFixed.div(ydiff, distance)
#(rotated) axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVy), SGFixed.mul(dirspeed, -aVx))
# sprite checks
	# if the player is going through an attack animation, they are not blocking by definition
	# if the player is moving forward or laterally, they aren't blocking
	if input_vector == Vector2(0, 1) || input_vector == Vector2(0, -1):
		blockingState = false
		movingState = true
		animsprite.animation = "moving"
	if input_vector == Vector2(-1, 0):
		if posSwitch == false:
			blockingState = true
			movingState = false
			animsprite.animation = "blocking"
		else:
			blockingState = false
			movingState = true
			animsprite.animation = "moving"
	if input_vector == Vector2(1, 0):
		if posSwitch == false:
			blockingState = false
			movingState = true
			animsprite.animation = "moving"
		else:
			blockingState = true
			movingState = false
			animsprite.animation = "blocking"
# the player is blocking if they aren't doing anything
		if input_vector == Vector2.ZERO:
			if neverblock == false:
				blockingState = true
				movingState = false
				animsprite.animation = "blocking"
			else:
				blockingState = false
				movingState = true
				animsprite.animation = "moving"
		
# inverting directions 
	if clientplayer.get_global_fixed_position().x < self.get_global_fixed_position().x:
		posSwitch = true
	else:
		posSwitch = false

# direction changes
	if input.get("ub", false):
		if input_vector != Vector2(1, 0) && input_vector != Vector2(0, -1):
			if posSwitch == false:
				direction = SGFixed.ONE
			else:
				direction = SGFixed.NEG_ONE
	if input.get("df", false):
		if input_vector != Vector2(-1, 0) && input_vector != Vector2(0, 1):
			if posSwitch == false:
				direction = SGFixed.NEG_ONE
			else:
				direction = SGFixed.ONE
# movement
	var dirspeed = SGFixed.mul(direction, speed)
	var axisVector = self.get_global_fixed_position().direction_to(clientplayer.get_global_fixed_position()).normalized().copy()	
	if input_vector != Vector2.ZERO && deadState == false:
		if input_vector.y == 0:
			var velocity = move_and_slide(axisVector.mul(-dirspeed)).copy()
		else:
			var velocity = move_and_slide(axisVector.mul(dirspeed).rotated(-97000)).copy()
# pushback
#	if animtimer.currentdefense != "":
#		match animtimer.currentdefense:
#			"deathfistknockdown":
#				velocity = move_and_slide(axisVector.mul(-349999))
#			"homingknockdown":
#				velocity = move_and_slide(axisVector.mul(-299999))
#			"overheadblockstun":
#				velocity = move_and_slide(axisVector.mul(-99999))
#			"overheadknockdown":
#				velocity = move_and_slide(axisVector.mul(-349999))
#			"jabhitstun":
#				velocity = move_and_slide(axisVector.mul(-99999))
#			"jabblockstun":
#				velocity = move_and_slide(axisVector.mul(-19999))
#			"ucuthitstun":
#				velocity = move_and_slide(axisVector.mul(-99999))
#			"ucutblockstun":
#				velocity = move_and_slide(axisVector.mul(-29999))
#			"wallsplat":
#				velocity = move_and_slide(axisVector.mul(49999))
#			"throwknockdown":
#				velocity = move_and_slide(axisVector.mul(-349999))
# rotation functions
	set_global_fixed_rotation(angle)



