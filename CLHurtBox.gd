extends Area2D

onready var animtimer = $"../AttackTimer"
func _network_process(input: Dictionary) -> void:
	if animtimer.phase == "recovery":
		$recovery.disabled = false
	else:
		$recovery.disabled = true

func _save_state() -> Dictionary:
	return {
		recoverybox = $recovery.disabled,
	}

func _load_state(state: Dictionary) -> void:
	$recovery.disabled = state['recoverybox']
