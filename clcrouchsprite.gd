extends AnimatedSprite

onready var cl = get_parent()
onready var animsprite = $"../clanimsprite"
onready var victimsprite = $"../clvictimsprite"
func _process(delta):
	if animsprite.is_playing() == false && victimsprite.is_playing() == false && cl.crouchState == true:
		self.visible = true
		animsprite.visible = false
	else:
		self.visible = false
		
