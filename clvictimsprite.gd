extends AnimatedSprite
onready var cl = get_parent()
onready var animsprite = $"../clanimsprite"
onready var victimsprite = $"../clvictimsprite"
func _process(delta):
	if cl.deadState == true:
		self.visible = true
		self.animation = "death"
		animsprite.stop()
		animsprite.visible = false
	if self.is_playing():
		animsprite.stop()
		self.visible = true
		animsprite.visible = false
	else:
		self.visible = false
		
