extends Sprite

onready var parking_lot = preload("res://backgrounds/bg1.png")
onready var cloudbusting = preload("res://backgrounds/bg2.png")
onready var floorplan = preload("res://backgrounds/bg3.png")
onready var silicon = preload("res://backgrounds/bg4.png")
onready var upzoning = preload("res://backgrounds/bg5.png")
onready var verdant = preload("res://backgrounds/bg6.png")
onready var solar = preload("res://backgrounds/bg7.png")
onready var corn = preload("res://backgrounds/bg8.png")
onready var swamplands = preload("res://backgrounds/bg9.png")
onready var tiles = preload("res://backgrounds/bg10.png")
onready var mariposa_heights = preload("res://backgrounds/bg11.png")
onready var permafrost = preload("res://backgrounds/bg12.png")
onready var bay = preload("res://backgrounds/bg13.png")
onready var ingria = preload("res://backgrounds/bg14.png")
onready var abyss = preload("res://backgrounds/bg15.png")
onready var accept = preload('res://backgrounds/bg16.png')
onready var reject = preload('res://backgrounds/bg17.png')
onready var blind = preload('res://backgrounds/bg18.png')
onready var bloom = preload('res://backgrounds/bg19.png')
onready var pilgrim = preload('res://backgrounds/bg20.png')
onready var ornate = preload('res://backgrounds/bg21.png')
onready var edge = preload('res://backgrounds/bg22.png')
onready var gaze = preload('res://backgrounds/bg23.png')

func _on_ItemList_item_selected(index):
	match index:
		0:
			texture = parking_lot
		1:
			texture = cloudbusting
		2:
			texture = floorplan
		3:
			texture = silicon
		4:
			texture = upzoning
		5:
			texture = verdant
		6:
			texture = solar
		7:
			texture = corn
		8:
			texture = swamplands
		9:
			texture = tiles
		10:
			texture = mariposa_heights
		11:
			texture = permafrost
		12:
			texture = bay
		13:
			texture = ingria
		14:
			texture = abyss
		15:
			texture = accept
		16:
			texture = reject
		17:
			texture = blind
		18:
			texture = bloom
		19:
			texture = pilgrim
		20:
			texture = ornate
		21:
			texture = edge
		22:
			texture = gaze
