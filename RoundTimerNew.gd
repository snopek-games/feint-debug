extends "res://addons/godot-rollback-netcode/NetworkTimer.gd"

onready var sv = $"/root/Main/ServerPlayer"
onready var cl = $"/root/Main/ClientPlayer"
onready var roundender = $"/root/Main/EndSplash/REControl/RoundEnder"

signal loser(identity)

func _ready():
	wait_ticks = 1800

func _network_process(input: Dictionary) -> void:
	if not _running:
		return
	if ticks_left <= 0:
		_running = false
		return
	
	ticks_left -= 1
	
	if ticks_left == 0:
		if not one_shot:
			ticks_left = wait_ticks
		emit_signal("timeout")

func _on_roundover():
	wait_ticks = 120
	start()


func _on_EnderTimer_roundchange():
	wait_ticks = 1800
	start()

func on_timeout():
	print("round timeout")
	if sv.health == cl.health:
		emit_signal("loser", "draw")
	if sv.health > cl.health:
		emit_signal("loser", "client")
	if sv.health < cl.health:
		emit_signal("loser", "server")




func _on_RoundTimer_loser(identity):
	match identity:
		"server":
			print("server lost")
			sv.roundsLeft -= 1
			if sv.roundsLeft > 0:
				roundender.play("P2ROUNDWIN")
			else:
				roundender.play("P2GAMEWIN")
				sv.roundsLeft = 3
				cl.roundsLeft = 3
		"client":
			cl.roundsLeft -= 1
			if cl.roundsLeft > 0:
				roundender.play("P1ROUNDWIN")
			else:
				roundender.play("P1GAMEWIN")
				cl.roundsLeft = 3
				sv.roundsLeft = 3
		"draw":
			cl.roundsLeft -= 1
			sv.roundsLeft -= 1
			if sv.roundsLeft == 0 && cl.roundsLeft == 0:
				roundender.play("DRAWEND")
				cl.roundsLeft = 3
				sv.roundsLeft = 3
			elif sv.roundsLeft == 0:
				roundender.play("P2GAMEWIN")
				sv.roundsLeft = 3
				cl.roundsLeft = 3
			elif cl.roundsLeft == 0:
				roundender.play("P1GAMEWIN")
				sv.roundsLeft = 3
				cl.roundsLeft = 3
			elif sv.roundsLeft > 0 && cl.roundsLeft > 0:
					roundender.play("DRAW")

func _save_state() -> Dictionary:
	if hash_state:
		return {
			running = _running,
			wait_ticks = wait_ticks,
			ticks_left = ticks_left,
		}
	else:
		return {
			_running = _running,
			_wait_ticks = wait_ticks,
			_ticks_left = ticks_left,
		}

func _load_state(state: Dictionary) -> void:
	if hash_state:
		_running = state['running']
		wait_ticks = state['wait_ticks']
		ticks_left = state['ticks_left']
	else:
		_running = state['_running']
		wait_ticks = state['_wait_ticks']
		ticks_left = state['_ticks_left']
