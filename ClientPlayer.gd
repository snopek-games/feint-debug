extends SGKinematicBody2D
# local variables
var health = 150
var roundsLeft = 3
var buffered = ""
var blockingState = true
var crouchState = false
var movingState = false
var netstatus = "client"
var posSwitch = false
var deadState = false
var counterState = false
var input_prefix := "player1_"
var behindYou = false
var tooclose = false
var distancelimit = SGFixed.ONE
var damaged = ""
# fixed point variables
var svpos = SGFixedVector2.new().copy()
var clpos = SGFixedVector2.new().copy()
var velocity = SGFixedVector2.new().copy()
var axisVector = SGFixedVector2.new().copy()
var direction = SGFixed.ONE
var speed = 444444
var clrot = SGFixed.ONE
var svrot = SGFixed.ONE
var angle = int(1)
var dirspeed = SGFixed.ONE
var q = 1
# node paths
onready var serverplayer = $"../ServerPlayer"
onready var clientplayer = $"../ClientPlayer"
onready var animsprite = $clanimsprite
# signals
# bot variables
var neverblock = false
var neverspin = false
func _ready():
	self.fixed_position = SGFixed.vector2(50331648, 33554432).copy()
	set_global_fixed_rotation(205887 + angle)


func _save_state() -> Dictionary:
	return {
		clpos = self.get_global_fixed_position().copy(),
		velocity = velocity.copy(),
		direction = direction,
		isblocking = blockingState,
		ismoving = movingState,
		speed = speed,
		switched = posSwitch,
	}

func _load_state(state: Dictionary) -> void:
	self.set_global_fixed_position(state['clpos'].copy())
	velocity = state['velocity'].copy()
	direction = state['direction']
	blockingState = state['isblocking']
	movingState = state['ismoving']
	speed = state['speed']
	posSwitch = state['switched']
	sync_to_physics_engine()

func _get_local_input() -> Dictionary:
	var input_vector = Input.get_vector(input_prefix + "back", input_prefix + "forward", input_prefix +  "down", input_prefix +  "up")
	var input := {}
	if input_vector != Vector2.ZERO:
		input["input_vector"] = input_vector
	if Input.is_action_pressed(input_prefix +"crouch"):
		input["crouch"] = true
	if Input.is_action_just_pressed(input_prefix + "up") || Input.is_action_just_pressed(input_prefix + "back") || Input.is_action_just_released(input_prefix + "down") || Input.is_action_just_released(input_prefix + "forward"):
		input["ub"] = true
	if Input.is_action_just_pressed(input_prefix + "down") || Input.is_action_just_pressed(input_prefix + "forward") || Input.is_action_just_released(input_prefix + "up") || Input.is_action_just_released(input_prefix + "back"):
		input["df"] = true
	if Input.is_action_just_pressed(input_prefix + "light_attack"):
		input["la"] = true
	if Input.is_action_just_pressed(input_prefix + "heavy_attack"):
		input["ha"] = true
	return input

func _network_process(input: Dictionary) -> void:
	var input_vector = input.get("input_vector", Vector2.ZERO)
#	print(altvel.x)
#	print(float(angle)/65536*180/PI)
#	print(float(self.get_global_fixed_rotation())/65536*180/PI)
# player rotations
	svrot = serverplayer.get_global_fixed_rotation()
	clrot = self.get_global_fixed_rotation()
# behind you check
	var sveyevector = SGFixed.vector2(SGFixed.cos(svrot), SGFixed.sin(svrot)).copy()
	var cleyevector = SGFixed.vector2(SGFixed.cos(clrot), SGFixed.sin(clrot)).copy()
	if sveyevector.angle_to(cleyevector) > -100000 && sveyevector.angle_to(cleyevector) < 100000:
		behindYou = true
	else:
		behindYou = false
# use that to calculate the angle and distance between players
	angle = SGFixed.atan2(
		self.get_global_fixed_position().y - serverplayer.get_global_fixed_position().y,
		self.get_global_fixed_position().x - serverplayer.get_global_fixed_position().x
	)
	if angle < 0 && angle > -102943:
		q = 1
	if angle < -102943 && angle > -205887:
		q = 2
	if angle < 205887 && angle > 102943:
		q = 3
	if angle < 102943 && angle > 0:
		q = 4
	var distance = self.get_global_fixed_position().distance_to(serverplayer.get_global_fixed_position())
# sprite checks
	# if the player is going through an attack animation, they are not blocking by definition
	# if the player is moving forward or laterally, they aren't blocking
	if input_vector == Vector2(0, 1) || input_vector == Vector2(0, -1):
		blockingState = false
		movingState = true
		animsprite.animation = "moving"
	if input_vector == Vector2(-1, 0):
		if posSwitch == false:
			blockingState = false
			movingState = true
			animsprite.animation = "moving"
		else:
			blockingState = true
			movingState = false
			animsprite.animation = "blocking"
	if input_vector == Vector2(1, 0):
		if posSwitch == false:
			blockingState = true
			movingState = false
			animsprite.animation = "blocking"
		else:
			blockingState = true
			movingState = false
			animsprite.animation = "moving"
# the player is blocking if they aren't doing anything
	if input_vector == Vector2.ZERO:
		if neverblock == false:
			blockingState = true
			movingState = false
			animsprite.animation = "blocking"
		else:
			blockingState = false
			movingState = true
			animsprite.animation = "moving"
		
# inverting directions 
	if self.get_global_fixed_position().x < serverplayer.get_global_fixed_position().x:
		posSwitch = true
	else:
		posSwitch = false


# direction switches
	if input.get("ub", false):
		if input_vector != Vector2(1, 0) && input_vector != Vector2(0, -1):
			if posSwitch == false:
				direction = SGFixed.ONE
			else:
				direction = SGFixed.NEG_ONE
	if input.get("df", false):
		if input_vector != Vector2(-1, 0) && input_vector != Vector2(0, 1):
			if posSwitch == false:
				direction = SGFixed.NEG_ONE
			else:
				direction = SGFixed.ONE
# movement
	var axisVector = self.get_global_fixed_position().direction_to(serverplayer.get_global_fixed_position()).normalized().copy()
	var dirspeed = SGFixed.mul(direction, speed)
	if input_vector != Vector2.ZERO && deadState == false:
		if input_vector.y == 0:
			var velocity = move_and_slide(axisVector.mul(dirspeed)).copy()
		else:
			var velocity = move_and_slide(axisVector.mul(-dirspeed).rotated(-97000)).copy()
# pushback
#	if animtimer.currentdefense != "":
#		match animtimer.currentdefense:
#			"deathfistknockdown":
#				velocity = move_and_slide(axisVector.mul(349999))
#			"homingknockdown":
#				velocity = move_and_slide(axisVector.mul(299999))
#			"overheadblockstun":
#				velocity = move_and_slide(axisVector.mul(99999))
#			"overheadknockdown":
#				velocity = move_and_slide(axisVector.mul(349999))
#			"jabhitstun":
#				velocity = move_and_slide(axisVector.mul(99999))
#			"jabblockstun":
#				velocity = move_and_slide(axisVector.mul(19999))
#			"ucuthitstun":
#				velocity = move_and_slide(axisVector.mul(99999))
#			"ucutblockstun":
#				velocity = move_and_slide(axisVector.mul(29999))
#			"wallsplat":
#				velocity = move_and_slide(axisVector.mul(-349999))
# rotation functions
	if neverspin == false:
		match q:
			1:
				set_global_fixed_rotation(205887 + angle)
			2: 
				set_global_fixed_rotation(-205887 + angle)
			3:
				set_global_fixed_rotation(-205887 + angle)
			4:
				set_global_fixed_rotation(205887 + angle)
