extends "res://addons/godot-rollback-netcode/NetworkTimer.gd"

onready var currentatk = ""
onready var currentdefense = ""
onready var animsprite = $"../svanimsprite"
onready var victimsprite = $"../svvictimsprite"
onready var hitbox = $"../SVHITBOX"
onready var sv = get_parent()
var phase = ""
func _network_process(input: Dictionary) -> void:
	if not _running:
		return
	if ticks_left <= 0:
		_running = false
		return
	
	ticks_left -= 1
	
	if ticks_left == 0:
		if not one_shot:
			ticks_left = wait_ticks
		emit_signal("timeout")
	
	match currentatk:
		"jab":
			if ticks_left >= 18:
				phase = "startup"
			elif ticks_left <= 17 && ticks_left >= 11:
				phase = "active"
			elif ticks_left <= 10:
				phase = "recovery"
		"ucut":
			if ticks_left >= 22:
				phase = "startup"
			elif ticks_left <= 21 && ticks_left >= 15:
				phase = "active"
			elif ticks_left <= 14:
				phase = "recovery"
		"low":
			if ticks_left >= 22:
				phase = "startup"
			elif ticks_left <= 21 && ticks_left >= 15:
				phase = "active"
			elif ticks_left <= 14:
				phase = "recovery"
		"homing":
			if ticks_left >= 24:
				phase = "startup"
			elif ticks_left <= 23 && ticks_left >= 17:
				phase = "active"
			elif ticks_left <= 16:
				phase = "recovery"
		"hook":
			if ticks_left >= 24:
				phase = "startup"
			elif ticks_left <= 23 && ticks_left >= 17:
				phase = "active"
			elif ticks_left <= 16:
				phase = "recovery"
		"deathfist":
			if ticks_left >= 31:
				phase = "startup"
			elif ticks_left <= 30 && ticks_left >= 24:
				phase = "active"
			elif ticks_left <= 23:
				phase = "recovery"
		"throw":
			if ticks_left >= 6:
				phase = "startup"
			else:
				phase = "active"
		"overhead":
			if ticks_left >= 25:
				phase = "startup"
			elif ticks_left <= 24 && ticks_left >= 18:
				phase = "active"
			elif ticks_left <= 17:
				phase = "recovery"
		_:
			phase = "neutral"

func _on_ServerPlayer_attack_event(attack):
	if sv.deadState == false:
		self.stop()
		currentatk = attack
		wait_ticks = animsprite.frames.get_frame_count(attack)
		self.start()

func _on_AttackTimer_timeout():
	currentatk = ""
	currentdefense = ""
	self.stop()

func _on_ServerPlayer_defense_event(defense):
	if sv.deadState == false:
		currentatk = ""
		phase = ""
		self.stop()
		currentdefense = defense
		wait_ticks = victimsprite.frames.get_frame_count(defense)
		self.start()

func _save_state() -> Dictionary:
	if hash_state:
		return {
			running = _running,
			wait_ticks = wait_ticks,
			ticks_left = ticks_left,
			currentatk = currentatk,
			currentdefense = currentdefense,
			monitoring = hitbox.monitoring,
			counter = sv.counterState,
		}
	else:
		return {
			_running = _running,
			_wait_ticks = wait_ticks,
			_ticks_left = ticks_left,
			_currentatk = currentatk,
			_currentdefense = currentdefense,
			_monitoring = hitbox.monitoring,
			_counter = sv.counterState,
		}

func _load_state(state: Dictionary) -> void:
	if hash_state:
		_running = state['running']
		wait_ticks = state['wait_ticks']
		ticks_left = state['ticks_left']
		currentatk = state['currentatk']
		currentdefense = state['currentdefense']
		hitbox.monitoring = state['monitoring']
		sv.counterState = state['counter']
	else:
		_running = state['_running']
		wait_ticks = state['_wait_ticks']
		ticks_left = state['_ticks_left']
		currentatk = state['_currentatk']
		currentdefense = state['_currentdefense']
		hitbox.monitoring = state['_monitoring']
		sv.counterState = state['_counter']
