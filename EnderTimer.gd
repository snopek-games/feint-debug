extends "res://addons/godot-rollback-netcode/NetworkTimer.gd"

signal roundchange


func _on_RoundTimer_timeout():
	wait_ticks = 120
	start()



func _on_EnderTimer_timeout():
	print("end screen stopped")
	emit_signal("roundchange")
	stop()

func _save_state() -> Dictionary:
	if hash_state:
		return {
			running = _running,
			wait_ticks = wait_ticks,
			ticks_left = ticks_left,
		}
	else:
		return {
			_running = _running,
			_wait_ticks = wait_ticks,
			_ticks_left = ticks_left,
		}

func _load_state(state: Dictionary) -> void:
	if hash_state:
		_running = state['running']
		wait_ticks = state['wait_ticks']
		ticks_left = state['ticks_left']
	else:
		_running = state['_running']
		wait_ticks = state['_wait_ticks']
		ticks_left = state['_ticks_left']
