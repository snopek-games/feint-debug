extends AnimatedSprite

onready var sv = get_parent()
onready var animsprite = $"../svanimsprite"
onready var victimsprite = $"../svvictimsprite"
func _process(delta):
	if animsprite.is_playing() == false && sv.crouchState == true && victimsprite.is_playing() == false:
		self.visible = true
		animsprite.visible = false
	else:
		self.visible = false
