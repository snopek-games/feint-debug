extends SGKinematicBody2D
# local variables
var health = 150
var roundsLeft = 3
var buffered = ""
var blockingState = true
var crouchState = false
var movingState = false
var netstatus = "server"
var posSwitch = false
var deadState = false
var counterState = false
var input_prefix := "player1_"
var behindYou = false
var neverblock = false
var lastpos = Vector2()
# fixed point variables
var svpos = SGFixedVector2.new()
var clpos = SGFixedVector2.new()
var velocity = SGFixedVector2.new()
var axisVector = SGFixedVector2.new()
var direction = SGFixed.ONE
var speed = 6666666
var dirspeed = int(1)
var xdiff = int(1)
var ydiff = int(1)
var distance = int(1)
var angle = int(1)
var aVx = int(1)
var aVy = int(1)
var q = 1
# node paths
onready var serverplayer = $"../ServerPlayer"
onready var clientplayer = $"../ClientPlayer"
onready var _animated_sprite = $svanimsprite
onready var victimsprite = $svvictimsprite
onready var timer = $"/root/Main/RoundTimer"
# signals
signal attack_event(attack)
signal buffer(bufferedAttack)
signal death
func _ready():
	print(axisVector)
	look_at(clientplayer.get_position())




	
func _save_state() -> Dictionary:
	return {
		svposx = fixed_position.x,
		svposy = fixed_position.y,
	}

func _load_state(state: Dictionary) -> void:
	fixed_position.x = state['svposx']	
	fixed_position.y = state['svposy']
	sync_to_physics_engine()


func _get_local_input() -> Dictionary:
	var input_vector = Input.get_vector(input_prefix + "back", input_prefix + "forward", input_prefix +  "up", input_prefix +  "down")
	var input := {}
	if input_vector != Vector2.ZERO:
		input["input_vector"] = input_vector
	if Input.is_action_pressed(input_prefix + "up"):
		input["up"] = true
	if Input.is_action_pressed(input_prefix + "down"):
		input["down"] = true
	if Input.is_action_pressed(input_prefix + "forward"):
		input["forward"] = true
	if Input.is_action_pressed(input_prefix + "back"):
		input["back"] = true
	if Input.is_action_pressed(input_prefix +"crouch"):
		input["crouch"] = true
#	if Input.is_action_just_pressed(input_prefix + "up") || Input.is_action_just_released(input_prefix + "down") || Input.is_action_just_released(input_prefix + "forward"):
#		input["u"] = true
#	if Input.is_action_just_pressed(input_prefix + "back") || Input.is_action_just_released(input_prefix + "down") || Input.is_action_just_released(input_prefix + "forward"):
#		input["b"] = true
#	if Input.is_action_just_pressed(input_prefix + "down") || Input.is_action_just_released(input_prefix + "up") || Input.is_action_just_released(input_prefix + "back"):
#		input["d"] = true
#	if Input.is_action_just_pressed(input_prefix + "forward") || Input.is_action_just_released(input_prefix + "back") || Input.is_action_just_released(input_prefix + "up"):
#		input["f"] = true
	if Input.is_action_just_pressed(input_prefix + "up") || Input.is_action_just_pressed(input_prefix + "back") || Input.is_action_just_released(input_prefix + "down") || Input.is_action_just_released(input_prefix + "forward"):
		input["ub"] = true
	if Input.is_action_just_pressed(input_prefix + "down") || Input.is_action_just_pressed(input_prefix + "forward") || Input.is_action_just_released(input_prefix + "up") || Input.is_action_just_released(input_prefix + "back"):
		input["df"] = true
	if Input.is_action_just_pressed(input_prefix + "light_attack"):
		input["la"] = true
	if Input.is_action_just_pressed(input_prefix + "heavy_attack"):
		input["ha"] = true
	return input

func _network_process(input: Dictionary) -> void:
# general checks
	if self.get_angle_to(clientplayer.get_position()) < -1 || self.get_angle_to(clientplayer.get_position()) > 1:
		behindYou = true
	else:
		behindYou = false
	if _animated_sprite.is_playing() || victimsprite.is_playing():
		speed = int(0)
	else: speed = int(6666666)
# calculate fixed point variables each frame
# take the player positions
	svpos = serverplayer.get_global_fixed_position()
	clpos = clientplayer.get_global_fixed_position()
# find the difference between coordinates
	xdiff = clpos.x - svpos.x
	ydiff = clpos.y - svpos.y
# use that to calculate the angle and distance between players
	angle = SGFixed.atan2(ydiff, xdiff)
	distance = SGFixed.sqrt(xdiff^2 + ydiff^2)
# use that to calculate the normalized vector between players
	aVx = SGFixed.div(SGFixed.cos(angle), distance)
	aVy = SGFixed.div(SGFixed.sin(angle), distance)
	#print(float(angle)/65536*180/PI)
#(rotated) axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVy), SGFixed.mul(dirspeed, -aVx))
	if angle < 0 && angle > -102943:
		q = 1
	if angle < -102943 && angle > -205887:
		q = 2
	if angle < 205887 && angle > 102943:
		q = 3
	if angle < 102943 && angle > 0:
		q = 4
# sprite checks
	# if the player is going through an attack animation, they are not blocking by definition
	if _animated_sprite.is_playing() || behindYou == true:
		blockingState = false
	# if the player is moving forward or laterally, they aren't blocking
	# movingState enables the static moving sprite
	if input.get("forward", false) || input.get("up", false) || input.get("down", false):
		blockingState = false
		movingState = true
	else:
		if neverblock == false:
			blockingState = true
			movingState = false
		else:
			blockingState = false
			movingState = true
	# the player is blocking if they aren't doing anything
	# check for crouching, you aren't blocking standing when crouching
	if input.get("crouch", false):
		crouchState = true
		blockingState = false
#		_static_sprite.play("crouching")
	else:
		crouchState = false
# attack buffering from other attacks
	var last_atk_frame = _animated_sprite.frames.get_frame_count(_animated_sprite.animation) - 1
	if last_atk_frame == _animated_sprite.get_frame():
		match buffered:
			"":
				emit_signal("buffer", "")
			"jab":
				emit_signal("buffer", "jab")
			"ucut":
				emit_signal("buffer", "ucut")
			"low":
				emit_signal("buffer", "low")
			"deathfist":
				emit_signal("buffer", "deathfist")
			"hook":
				emit_signal("buffer", "hook")
			"homing":
				emit_signal("buffer", "homing")
			"overhead":
				print("buffered overhead")
				emit_signal("buffer", "overhead")
# attack buffering from blockstun or hitstun
	var last_vtm_frame = victimsprite.frames.get_frame_count(victimsprite.animation) - 1
	if last_vtm_frame == victimsprite.get_frame():
		match buffered:
			"":
				emit_signal("buffer", "")
			"jab":
				emit_signal("buffer", "jab")
			"ucut":
				emit_signal("buffer", "ucut")
			"low":
				emit_signal("buffer", "low")
			"deathfist":
				emit_signal("buffer", "deathfist")
			"hook":
				emit_signal("buffer", "hook")
			"homing":
				emit_signal("buffer", "homing")
			"overhead":
				emit_signal("buffer", "overhead")
		
# inverting directions 
	if q == 2 || q == 3:
		posSwitch = true
	else:
		posSwitch = false
		
		
	# normal attacks
	if !_animated_sprite.is_playing() && !victimsprite.is_playing():
# jab / low
		if input.get("la", false):
			if !input.get("back", false) && !input.get("forward", false):
				print("neutral jab")
				emit_signal("attack_event", "jab")
			if input.get("back", false):
				match posSwitch:
					true:
						emit_signal("attack_event", "low")
					false:
						emit_signal("attack_event", "jab")
			if input.get("forward", false):
					match posSwitch:
						true:
							emit_signal("attack_event", "jab")
						false:
							emit_signal("attack_event", "low")
# hook / homing
		if input.get("ha", false):
			if !input.get("back", false) && !input.get("forward", false):
				emit_signal("attack_event", "hook")
			if input.get("back", false):
					match posSwitch:
						true:
							emit_signal("attack_event", "homing")
						false:
							emit_signal("attack_event", "hook")
			if input.get("forward", false):
					match posSwitch:
						true:
							emit_signal("attack_event", "hook")
						false:
							emit_signal("attack_event", "homing")
# deathfist / overhead / ucut
		if input.get("ha", false) && input.get("down", false):
			emit_signal("attack_event", "deathfist")
			print("deathfist")
		if input.get("ha", false) && input.get("up", false):
			emit_signal("attack_event", "overhead")
			print("overhead")
		if input.get("la", false) && input.get("down", false):
			emit_signal("attack_event", "ucut")
			print("ucut")
	# buffered attacks
	else:
# jab / low
		if input.get("la", false):
			if !input.get("back", false) && !input.get("forward", false):
				buffered = "jab"
			if input.get("back", false):
					match posSwitch:
						true:
							buffered = "low"
						false:
							buffered = "jab"
			if input.get("forward", false):
					match posSwitch:
						true:
							buffered = "jab"
						false:
							buffered = "low"
# hook / homing
		if input.get("ha", false):
			if !input.get("back", false) && !input.get("forward", false):
				buffered = "hook"
			if input.get("back", false):
					match posSwitch:
						true:
							buffered = "homing"
						false:
							buffered = "hook"
			if input.get("forward", false):
					match posSwitch:
						true:
							buffered = "hook"
						false:
							buffered = "homing"
		
		if input.get("ha", false) && input.get("down", false):
			buffered = "deathfist"
			print("deathfist")
		if input.get("ha", false) && input.get("up", false):
			buffered = "deathfist"
			print("overhead")
		if input.get("la", false) && input.get("down", false):
			buffered = "ucut"
			print("ucut")
# direction changes
	if input.get("ub", false):
		if !input.get("forward", false) && !input.get("down", false):
			if q != 3 || q != 2:
				direction = SGFixed.ONE
				print(direction)
			else:
				direction = SGFixed.NEG_ONE
				print(direction)
	if input.get("df", false):
		if !input.get("back", false) && !input.get("up", false):
			if q != 3 || q != 2:
				direction = SGFixed.NEG_ONE
				print(direction)
			else:
				direction = SGFixed.ONE
				print(direction)
# movement
	var input_vector = input.get("input_vector", Vector2.ZERO)
	if input_vector != Vector2.ZERO:
		dirspeed = SGFixed.mul(direction, speed)
		if input_vector.y == 0:
#			axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVx), SGFixed.mul(dirspeed, aVy))
			if q == 1 || q == 3:
				axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVx), SGFixed.mul(dirspeed, aVy))
			else:
				axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, -aVx), SGFixed.mul(dirspeed, -aVy))
			axisVector = move_and_collide(axisVector)
		else:
			if q == 1 || q == 3:
					axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, -aVy), SGFixed.mul(dirspeed, aVx))
			else:
					axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVy), SGFixed.mul(dirspeed, -aVx))
			axisVector = move_and_collide(axisVector)
# pushback
#	if victimsprite.is_playing() && victimsprite.get_frame() != 0:
#		if q == 1 || q == 3:
#			axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, aVx), SGFixed.mul(dirspeed, aVy))
#		else:
#			axisVector = SGFixed.vector2(SGFixed.mul(dirspeed, -aVx), SGFixed.mul(dirspeed, -aVy))
#		match victimsprite.animation:
#			"deathfistblockstun":
#				axisVector = move_and_slide(axisVector)
#			"deathfistknockdown":
#				axisVector = move_and_slide(axisVector)
#			"homingknockdown":
#				axisVector = move_and_slide(axisVector)
#			"overheadblockstun":
#				axisVector = move_and_slide(axisVector)
#			"overheadknockdown":
#				axisVector = move_and_slide(axisVector)
# rotation functions
	if _animated_sprite.is_playing() || victimsprite.is_playing():
		var lastxdiff = clpos.x - lastpos.x
		var lastydiff = clpos.y - lastpos.y
		var lastangle = SGFixed.atan2(ydiff, xdiff)
		match q:
			1:
				set_global_fixed_rotation(lastangle)
			2: 
				set_global_fixed_rotation(lastangle)
			3:
				set_global_fixed_rotation(lastangle)
			4:
				set_global_fixed_rotation(lastangle)
	else:
		match q:
			1:
				set_global_fixed_rotation(angle)
			2: 
				set_global_fixed_rotation(angle)
			3:
				set_global_fixed_rotation(angle)
			4:
				set_global_fixed_rotation(angle)
func attack_end(bufferedAttack):
	if victimsprite.animation == "death":
		victimsprite.stop()
		victimsprite.frame = 0
	if buffered == "" && bufferedAttack == "" && victimsprite.animation != "death":
		print("ending")
		_animated_sprite.stop()
		victimsprite.stop()
		_animated_sprite.frame = 0
		victimsprite.frame = 0
	else:
		match buffered:
			"ucut":
				emit_signal("attack_event", "ucut")
			"jab":
				emit_signal("attack_event", "jab")
			"low":
				emit_signal("attack_event", "low")
			"deathfist":
				emit_signal("attack_event", "deathfist")
			"hook":
				emit_signal("attack_event", "hook")
			"homing":
				emit_signal("attack_event", "homing")
			"overhead":
				emit_signal("attack_event", "overhead")

func attack_handler(attack):
	lastpos = clientplayer.get_global_fixed_position()
	match attack:
		"jab":
			print("jab attack")
			_animated_sprite.play("jab")
			_animated_sprite.frame = 0
			buffered = ""
		"ucut":
			print("uppercut attack")
			_animated_sprite.play("ucut")
			_animated_sprite.frame = 0
			buffered = ""
		"low":
			print("low attack")
			_animated_sprite.play("low")
			_animated_sprite.frame = 0
			buffered = ""
		"deathfist":
			print("deathfist attack")
			_animated_sprite.play("deathfist")
			_animated_sprite.frame = 0
			buffered = ""
		"hook":
			print("hook attack")
			_animated_sprite.play("hook")
			_animated_sprite.frame = 0
			buffered = ""
		"homing":
			print("homing attack")
			_animated_sprite.play("homing")
			_animated_sprite.frame = 0
			buffered = ""
		"overhead":
			print("overhead attack")
			_animated_sprite.play("overhead")
			_animated_sprite.frame = 0
			buffered = ""

func damage(attacktype):
	lastpos = clientplayer.get_global_fixed_position()
	print("damage called")
	match attacktype:
		"jab":
			if crouchState == true:
				pass
			else:	
				if blockingState == false || behindYou == true:
					health -= 10
					print("i got jabbed (())")
					print(health)
					if health <= 0 && deadState != true:
						emit_signal("death")
					else:
						victimsprite.play("jabhitstun")
				else:
					victimsprite.play("jabblockstun")
		"hook":
			if crouchState == true:
				pass
			else:
				if blockingState == false || behindYou == true:
					if counterState == true || behindYou == true:
						victimsprite.play("hookknockdown")
						health -= 50
						print("strong hook")
						if health <= 0 && deadState != true:
							emit_signal("death")
						else:
								victimsprite.play("hookhitstun")
					else:
						health -= 15
						if health <= 0 && deadState != true:
							emit_signal("death")
				else:
					victimsprite.play("hookblockstun")
		"deathfist":
			print("deathfist hit clientplayer")
			if blockingState == false || behindYou == true:
				health -= 50
				print(health)
				if health <= 0:
						emit_signal("death")
				else:
					victimsprite.play("deathfistknockdown")
			else:
				victimsprite.play("deathfistblockstun")
		"ucut":
			if blockingState == false || behindYou == true:
				health -= 15
				print(health)
				if health <= 0 && deadState != true:
					emit_signal("death")
				else:
					victimsprite.play("ucuthitstun")
			else:
					victimsprite.play("ucutblockstun")
		"low":
			if crouchState == false || behindYou == true:
				health -= 20
				print(health)
				print("low hit")
				if health <= 0 && deadState != true:
					emit_signal("death")
				else:
					victimsprite.play("lowhitstun")
			else:
				print("")
				victimsprite.play("lowblockstun")
		"overhead":
			if blockingState == false || behindYou == true:
				print("not blocking")
				if counterState == true || behindYou == true:
					health -=50
					if health <= 0 && deadState != true:
						emit_signal("death")
					else:
						victimsprite.play("deathfistknockdown")
				else:
					print("normal overhead hit")
					health -= 20
					if health <= 0 && deadState != true:
						emit_signal("death")
					else:
						victimsprite.play("overheadhitstun")
						print(health)
			else:
					print("i blocked the overhead")
					victimsprite.play("overheadblockstun")
		"homing":
			if blockingState == false || behindYou == true:
				if counterState == true || behindYou == true:
					health -= 30
					print(health)
					if health <= 0 && deadState != true:
						emit_signal("death")
					else:
						victimsprite.play("homingknockdown")
				else: 
					health -= 20
					if health <= 0 && deadState != true:
						emit_signal("death")
					else:
						victimsprite.play("homingknockdown")
			else:
					print("i blocked the homing")
					victimsprite.play("homingblockstun")
func death():
	print("death function called")
	deadState = true
	speed = 0
	victimsprite.play("death")
	timer.set_wait_time(1)
	timer.start()
	
func _on_Main_roundchange():
	self.fixed_position = SGFixed.vector2(16777216, 19595264)
	sync_to_physics_engine()


func _on_svhurtbox_area_entered(area):
	pass # Replace with function body.
