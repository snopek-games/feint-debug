extends Sprite

onready var cl = get_parent()
onready var animsprite = $"../clanimsprite"
onready var victimsprite = $"../clvictimsprite"
func _process(delta):
	if !animsprite.is_playing() && !victimsprite.is_playing() && cl.blockingState == true:
		self.visible = true
	else:
		self.visible = false
		
