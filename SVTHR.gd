extends Area2D
onready var animsprite = $"../svanimsprite"
signal attackconnected(attacktype)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.overlaps_area($"/root/Main/ClientPlayer/clhurtbox"):
		print("throw connected")
		emit_signal("attackconnected", "throw")
	if animsprite.animation == "anklepick":
		match animsprite.get_frame():
			29:
				self.monitoring = true
			30:
				self.monitoring = true
			31:
				self.monitoring = true
			32:
				self.monitoring = true
			_:
				self.monitoring = false
	else:
		self.monitoring = false

	
