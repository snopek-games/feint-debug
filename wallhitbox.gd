extends Area2D
onready var sv_timer = $"/root/Main/ServerPlayer/AttackTimer"
onready var cl_timer = $"/root/Main/ClientPlayer/AttackTimer"
onready var sv = $"/root/Main/ServerPlayer"
onready var cl = $'/root/Main/ClientPlayer'
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if self.overlaps_area($"/root/Main/ServerPlayer/svhurtbox"):
		match sv_timer.currentdefense:
			"deathfistknockdown":
				print("wallsplat imminent")
				sv.damaged = "wallsplat"
			"hookknockdown":
				sv.damaged = "wallsplat"
			"overheadknockdown":
				sv.damaged = "wallsplat"
			"homingknockdown":
				sv.damaged = "wallsplat"
			_:
				pass
	if self.overlaps_area($"/root/Main/ClientPlayer/clhurtbox"):
		match cl_timer.currentdefense:
			"deathfistknockdown":
				cl.damaged = "wallsplat"
			"hookknockdown":
				cl.damaged = "wallsplat"
			"overheadknockdown":
				cl.damaged = "wallsplat"
			"homingknockdown":
				cl.damaged = "wallsplat"
			_:
				pass

	
