extends Area2D
onready var cl = $"/root/Main/ClientPlayer"
onready var cltimer = $'/root/Main/ClientPlayer/AttackTimer'
onready var animsprite = $"../svanimsprite"
onready var attacktimer = $"../AttackTimer"
onready var currentatk = ""
onready var clhurtbox = $"/root/Main/ClientPlayer/clhurtbox"
onready var animtimer = $"../AttackTimer"
func _ready():
	attacktimer.connect("timeout", self, "_on_AttackTimer_timeout")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _network_process(input: Dictionary) -> void:
	if animtimer.phase == "active":
		self.monitoring = true
	else:
		self.monitoring = false
	if self.overlaps_area($"/root/Main/ClientPlayer/clhurtbox") && cltimer.currentdefense == "":
		cl.damaged = currentatk

func _on_AttackTimer_timeout():
	print("attack timer ended")
	self.monitoring = false
	$short.disabled = true
	$medium.disabled = true
	$long.disabled = true
	$homing.disabled = true

func _on_ServerPlayer_attack_event(attack):
	currentatk = attack
	match attack:
		"jab":
			$short.set_deferred("disabled", false)
		"ucut":
			$medium.set_deferred("disabled", false)
		"low":
			$long.set_deferred("disabled", false)
		"throw":
			$long.set_deferred("disabled", false)
		"hook":
			$short.set_deferred("disabled", false)
		"deathfist":
			$long.set_deferred("disabled", false)
		"homing":
			$homing.set_deferred("disabled", false)
		"overhead":
			$long.set_deferred("disabled", false)
		
func _save_state() -> Dictionary:
	return {
		monitoring = monitoring
	}

func _load_state(state: Dictionary) -> void:
	self.monitoring = state['monitoring']


