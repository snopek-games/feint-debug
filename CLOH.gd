extends Area2D
onready var cl = get_parent()
onready var animsprite = $"../clanimsprite"
signal attackconnected(attacktype)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.overlaps_area($"/root/Main/ServerPlayer/svhurtbox"):
		emit_signal("attackconnected", "overhead")
	if animsprite.animation == "overhead":
		match animsprite.get_frame():
			19:
				self.monitoring = true
			20:
				self.monitoring = true
			21:
				self.monitoring = true
			22:
				self.monitoring = true
			_:
				self.monitoring = false
	else:
		self.monitoring = false

	
