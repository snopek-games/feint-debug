extends Area2D
onready var animsprite = $"../svanimsprite"
signal attackconnected(attacktype)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.overlaps_area($"/root/Main/ClientPlayer/clhurtbox"):
		emit_signal("attackconnected", "homing")
	if animsprite.animation == "homing":
		match animsprite.get_frame():
			14:
				self.monitoring = true
			15:
				self.monitoring = true
			16:
				self.monitoring = true
			17:
				self.monitoring = true
			_:
				self.monitoring = false
	else:
		self.monitoring = false

	
