extends Area2D
onready var cl = get_parent()
onready var sv = $"/root/Main/ServerPlayer"
onready var animsprite = $"../clanimsprite"
onready var animtimer = $"../AttackTimer"
onready var currentatk = ""
func _ready():
	animtimer.connect("timeout", self, "_on_AttackTimer_timeout")
	self.monitoring = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _network_process(input: Dictionary) -> void:
	if animtimer.phase == "active":
		self.monitoring = true
	else:
		self.monitoring = false
	if self.overlaps_area($"/root/Main/ServerPlayer/svhurtbox"):
		sv.damaged = currentatk

func _on_AttackTimer_timeout():
	self.monitoring = false
	$short.disabled = true
	$medium.disabled = true
	$long.disabled = true
	$homing.disabled = true

func _on_ClientPlayer_attack_event(attack):
	currentatk = attack
	match attack:
		"jab":
			$short.set_deferred("disabled", false)
		"ucut":
			$medium.set_deferred("disabled", false)
		"low":
			$long.set_deferred("disabled", false)
		"throw":
			$long.set_deferred("disabled", false)
		"hook":
			$short.set_deferred("disabled", false)
		"deathfist":
			$long.set_deferred("disabled", false)
		"homing":
			$homing.set_deferred("disabled", false)
		"overhead":
			$long.set_deferred("disabled", false)
		
			


func _save_state() -> Dictionary:
	return {
		monitoring = monitoring
	}

func _load_state(state: Dictionary) -> void:
	self.monitoring = state['monitoring']
