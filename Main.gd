extends SGFixedNode2D
# physics variables
var serverpos = Vector2()
var clientpos = Vector2()
var axisVector = Vector2()
var posSwitch = false
var logging_enabled := true
# net variables
onready var connection_panel = $CanvasLayer/ConnectionPanel
onready var main_menu = $CanvasLayer/MainMenu
onready var host_field = $CanvasLayer/ConnectionPanel/GridContainer/HostField
onready var port_field = $CanvasLayer/ConnectionPanel/GridContainer/PortField
onready var message_label = $CanvasLayer/MessageLabel
onready var sync_lost_label = $CanvasLayer/SyncLostLabel
onready var serverplayer = $ServerPlayer
onready var clientplayer = $ClientPlayer
onready var arenabg = $ArenaBG
onready var voidbg = $voidBG
onready var menubg = $CanvasLayer/MenuBG
onready var settings_panel = $CanvasLayer/SettingsPanel
onready var blocktoggle = $CanvasLayer/BlockToggle
onready var spintoggle = $CanvasLayer/RotationToggle
onready var charselect = $CanvasLayer/SettingsPanel/GridContainer/CharSelect
onready var charbuttons = $CanvasLayer/SettingsPanel/GridContainer/CharContainer
onready var wall = $Wall
onready var offline = false
onready var status = ""
onready var realbg = true
onready var fpslabel = $CanvasLayer/FPSLabel
onready var resetbutton = $CanvasLayer/ResetButton
const LOG_FILE_DIRECTORY = 'user://detailed_logs'
const DummyNetworkAdaptor = preload("res://addons/godot-rollback-netcode/DummyNetworkAdaptor.gd")


func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_on_network_peer_disconnected")
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")
	SyncManager.connect("sync_started", self, "_on_SyncManager_sync_started")
	SyncManager.connect("sync_stopped", self, "_on_SyncManager_sync_stopped")
	SyncManager.connect("sync_lost", self, "_on_SyncManager_sync_lost")
	SyncManager.connect("sync_regained", self, "_on_SyncManager_sync_regained")
	SyncManager.connect("sync_error", self, "_on_SyncManager_sync_error")



func _on_ServerButton_pressed() -> void:
	#johnny.randomize()
	status = "server"
	main_menu.visible = false
	menubg.visible = false
	connection_panel.visible = false
	offline = false
	settings_panel.visible = true
	main_menu.visible = false
	arenabg.visible = true
	charselect.visible = false
	charbuttons.visible = false

func _on_ClientButton_pressed() -> void:
#	var peer = NetworkedMultiplayerENet.new()
#	peer.create_client(host_field.text, int(port_field.text))
#	get_tree().network_peer = peer
	status = "client"
	main_menu.visible = false
	menubg.visible = false
	connection_panel.visible = false
	offline = false
	settings_panel.visible = true
	main_menu.visible = false
	arenabg.visible = true
	charselect.visible = false
	charbuttons.visible = false

func _on_network_peer_connected(peer_id: int):
	message_label.text = "Connected!"
	SyncManager.add_peer(peer_id)
	
	$ServerPlayer.set_network_master(1)
	if get_tree().is_network_server():
		$ClientPlayer.set_network_master(peer_id)
	else:
		$ClientPlayer.set_network_master(get_tree().get_network_unique_id())
	
	if get_tree().is_network_server():
		message_label.text = "Starting..."
		#rpc("setup_match", {mother_seed = johnny.get_seed()})
		
		# Give a little time to get ping data.
		yield(get_tree().create_timer(2.0), "timeout")
		SyncManager.start()



func _on_network_peer_disconnected(peer_id: int):
	message_label.text = "Disconnected"
	SyncManager.remove_peer(peer_id)
	
func _on_server_disconnected() -> void:
	_on_network_peer_disconnected(1)



func _on_ResetButton_pressed():
	SyncManager.stop()
	SyncManager.clear_peers()
	var peer = get_tree().network_peer
	if peer:
		peer.close_connection()
	get_tree().reload_current_scene()

func _on_SyncManager_sync_started() -> void:
	message_label.text = "Started!"
	if logging_enabled:
		var dir = Directory.new()
		if not dir.dir_exists(LOG_FILE_DIRECTORY):
			dir.make_dir(LOG_FILE_DIRECTORY)
		
		var datetime = OS.get_datetime(true)
		var log_file_name = "%04d%02d%02d-%02d%02d%02d-peer-%d.log" % [
			datetime['year'],
			datetime['month'],
			datetime['day'],
			datetime['hour'],
			datetime['minute'],
			datetime['second'],
			get_tree().get_network_unique_id(),
		]
		
		SyncManager.start_logging(LOG_FILE_DIRECTORY + '/' + log_file_name)
	

func _on_SyncManager_sync_stopped() -> void:
	if logging_enabled:
		SyncManager.stop_logging()
		
func _on_SyncManager_sync_lost() -> void:
	sync_lost_label.visible = true

func _on_SyncManager_sync_regained() -> void:
	sync_lost_label.visible = false

func _on_SyncManager_sync_error(msg: String) -> void:
	message_label.text = "Fatal sync error: " + msg
	sync_lost_label.visible = false
	
	var peer = get_tree().network_peer
	if peer:
		peer.close_connection()
	SyncManager.clear_peers()

func _on_SettingsButton_pressed():
	#johnny.randomize()
	if status == "server":
		var peer = NetworkedMultiplayerENet.new()
		peer.create_server(int(port_field.text), 1)
		get_tree().network_peer = peer
		message_label.text = "Listening..."
	elif status == "client":
		var peer = NetworkedMultiplayerENet.new()
		peer.create_client(host_field.text, int(port_field.text))
		get_tree().network_peer = peer
		message_label.text = "Connecting..."
	connection_panel.visible = false
	main_menu.visible = false
	menubg.visible = false
	settings_panel.visible = false
	arenabg.visible = true


	if offline == true:
		SyncManager.network_adaptor = DummyNetworkAdaptor.new()
		SyncManager.start()
	
func _on_OnlineButton_pressed():
	offline = false
	connection_panel.visible = true
	menubg.visible = false
	blocktoggle.visible = false
	spintoggle.visible = false
	arenabg.visible = true
	charselect.visible = false
	charbuttons.visible = false

func _on_LocalButton_pressed() -> void:
	print(realbg)
	main_menu.visible = false
	menubg.visible = false
	connection_panel.visible = false
	offline = true
	settings_panel.visible = true
	main_menu.visible = false
	blocktoggle.visible = true
	spintoggle.visible = true
	arenabg.visible = true
	charselect.visible = true
	charbuttons.visible = true
			


func _on_BlockToggle_toggled(button_pressed):
	if button_pressed == true:
		if $ServerPlayer.input_prefix == "player2_":
			$ServerPlayer.neverblock = true
		if $ClientPlayer.input_prefix == "player2_":
			$ClientPlayer.neverblock = true
	else:
		$ServerPlayer.neverblock = false
		$ClientPlayer.neverblock = false


func _on_RotationToggle_toggled(button_pressed):
	if button_pressed == true:
		if $ServerPlayer.input_prefix == "player2_":
			$ServerPlayer.neverspin = true
		if $ClientPlayer.input_prefix == "player2_":
			$ClientPlayer.neverspin = true
	else:
		$ServerPlayer.neverspin = false
		$ClientPlayer.neverspin = false

func _on_Purple_toggled(button_pressed):
	if button_pressed == true:
		$ClientPlayer.input_prefix = "player2_"
		$ServerPlayer.input_prefix = "player1_"
	else:
		$ClientPlayer.input_prefix = "player1_"
		$ServerPlayer.input_prefix = "player2_"


func _on_Green_toggled(button_pressed):
	if button_pressed == true:
		$ServerPlayer.input_prefix = "player2_"
		$ClientPlayer.input_prefix = "player1_"
	else:
		$ClientPlayer.input_prefix = "player1_"
		$ServerPlayer.input_prefix = "player2_"



		

