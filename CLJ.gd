extends Area2D
onready var cl = get_parent()
onready var animsprite = $"../clanimsprite"
signal attackconnected(attacktype)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.overlaps_area($"/root/Main/ServerPlayer/svhurtbox"):
		emit_signal("attackconnected", "jab")
	if animsprite.animation == "jab":
		match animsprite.get_frame():
			9:
				self.monitoring = true
			10:
				self.monitoring = true
			11:
				self.monitoring = true
			12:
				self.monitoring = true
			_:
				self.monitoring = false
	else:
		self.monitoring = false

	
