extends Sprite

onready var cl = get_parent()
onready var animsprite = $"../clanimsprite"
onready var victimsprite = $"../clvictimsprite"
func _process(delta):
	if animsprite.is_playing() == false && victimsprite.is_playing() == false && cl.movingState == true && cl.crouchState == false:
		self.visible = true
	else:
		self.visible = false
