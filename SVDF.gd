extends Area2D
onready var animsprite = $"../svanimsprite"
signal attackconnected(attacktype)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.overlaps_area($"/root/Main/ClientPlayer/clhurtbox"):
		print("deathfist connected")
		emit_signal("attackconnected", "deathfist")
	if animsprite.animation == "deathfist":
		match animsprite.get_frame():
			17:
				self.monitoring = true
			18:
				self.monitoring = true
			19:
				self.monitoring = true
			20:
				self.monitoring = true
			_:
				self.monitoring = false
	else:
		self.monitoring = false

	
