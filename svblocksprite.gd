extends Sprite

onready var sv = get_parent()
onready var animsprite = $"../svanimsprite"
onready var victimsprite = $"../svvictimsprite"
func _process(delta):
	if !animsprite.is_playing() && !victimsprite.is_playing() && sv.blockingState == true && sv.crouchState == false:
		self.visible = true
	else:
		self.visible = false
		
