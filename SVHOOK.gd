extends Area2D
onready var sv = get_parent()
onready var animsprite = $"../svanimsprite"
signal attackconnected(attacktype)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.overlaps_area($"/root/Main/ClientPlayer/clhurtbox"):
			emit_signal("attackconnected", "hook")
	if animsprite.animation == "hook":
		match animsprite.get_frame():
			11:
				self.monitoring = true
			12:
				self.monitoring = true
			13:
				self.monitoring = true
			14:
				self.monitoring = true
			_:
				self.monitoring = false
	else:
		self.monitoring = false
